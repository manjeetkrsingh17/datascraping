import {
  post,
  get
} from "axios";
import config from "../../config";

export async function scrape(url) {
  try {
    const response = await get(url);
    const data = response.data;
    return data;
  } catch (error) {
    console.log(error);
    throw new Error("Please try again later.");
  }
}