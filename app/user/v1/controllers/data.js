import config from "../../../config";

import {
  scrape
} from "../../../utils/v1/data-util";
const cheerio = require("cheerio");

import redis from 'redis';
let redisClient = redis.createClient();

redisClient.on("error", function (err) {
  console.log("Error " + err);
});

import {
  promisify
} from 'util';
const getAsync = promisify(redisClient.get).bind(redisClient);

export async function getData(req, res) {
  let url = req.body.url;
  try {
    let resObj = {};

    let urlScrape = await getAsync(url);

    if (urlScrape) {
      resObj = JSON.parse(urlScrape);
    } else {
      let data = await scrape(url);
      const $ = cheerio.load(data);
      let images = $("img");

      resObj = {
        title: $("head title").text(),
        description: $('meta[name="description"]').attr("content"),
        keywords: $('meta[name="keywords"]').attr("content"),
        ogImage: $('meta[property="og:image"]').attr("content"),
        ogTitle: $('meta[property="og:title"]').attr("content"),
        ogkeywords: $('meta[property="og:keywords"]').attr("content"),
        type: $('meta[property="og:type"]').attr("content"),
        url: $('meta[property="og:url"]').attr("content")
      }

      if (images && images.length) {
        resObj.images = [];

        for (var i = 0; i < images.length; i++) {
          resObj.images.push($(images[i]).attr("src"));
        }
      }

      redisClient.set(url, JSON.stringify(resObj), redis.print);
    }




    res.successWithData(resObj);
  } catch (error) {
    console.log(error);
    res.serverError("Something went wrong...");
  }
}