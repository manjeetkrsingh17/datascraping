import config from "../../config";
import {
    getData
} from "./controllers/data";

var router = require('express').Router();

router.post('/data', getData);

module.exports = router