$(document).ready(function () {
  var token;
  if (window.location.pathname !== "/" && localStorage.getItem('data')) {
    token = localStorage.getItem('data').token;
    let expiry = localStorage.getItem('data').timestamp;

    if (new Date().getTime() - expiry >= 3600000) {
      localStorage.removeItem('data');
      window.location.href = '/';
    }
  } else if (window.location.pathname !== "/") {
    window.location.href = '/';
  }

  $("#sidebarCollapse").on("click", function () {
    $("#sidebar").toggleClass("active");
  });

  let baseUrl = "/api/v1/";
  let table = [
    "ages",
    "domains",
    "subdomains",
    "functionalities",
    "subitemcategories",
    "categorycharacteristics",
    "taxonomyfamilies",
    "taxonomycategories",
    "taxonomykeywords",
    "taxonomymaps",
    "developmentaltasks",
    "ksas",
    "milestones",
    "subobjectives",
    "objectives",
    "indicators",
    "cases",
    "personalizations",
    "subitems",
    "caseksas",
    "items",
    "milestoneindicatorobjectives",
    "questions",
    "evaluationtypes",
    "responsetypes",
    "assessments",
    "contexts",
    "props",
    "activities",
    "roles",
    "casestatements",
    "casecontributions",
    "casemanifestations",
    "nurturequestions",
    "cohorts",
    "personas",
    "cohortinterpretations",
    "areaofinterests",
    "insights",
    "assessmentquestions",
    "articles",
    "skillinsights"
  ];

  table.sort()

  for (i = 0; i < table.length; i++) {
    $("ul#tableHeaders").append(
      '<li id="' +
      table[i] +
      '"><a href="#' +
      table[i] +
      '"><span class="tab" style="color:white;">' +
      table[i] +
      "</span></a></li>"
    );

    $("div#mainTableContent").append(
      '<section id="' +
      table[i] +
      '" class="tableContent"><h3 class="text-center" style="padding-bottom:10px;">' +
      table[i].toUpperCase() +
      ' Table</h4><table id="' +
      table[i] +
      'Table" class="table table-hover table-bordered display cmsTable"></table></section>'
    );
  }

  $("#mainTableContent")
    .children()
    .css("display", "none");

  $("#tableHeaders li").click(function (e) {
    e.preventDefault();
    if (localStorage.getItem('data')) {
      let expiry = JSON.parse(localStorage.getItem('data')).timestamp;

      if (new Date().getTime() - expiry >= 3600000) {
        localStorage.removeItem('data');
      }
    } else {
      window.location.href = '/';
    }

    $(".content-heading").css("display", "none");

    $("ul#tableHeaders > li").css("background", "#0957A5");

    $("ul#tableHeaders > li").find('span.tab').css("color", "white");

    $("#mainTableContent")
      .children()
      .css("display", "none");

    $(this)
      .closest("li")
      .css("background", "white");

    $(this)
      .closest("li").find('span.tab')
      .css("color", "#0957A5");

    let activeTable =
      table[
        $(this)
        .closest("li")
        .index()
      ];

    $.ajax({
      url: baseUrl + activeTable,
      headers: {
        'x-access-token': JSON.parse(localStorage.getItem('data')).token
      },
      success: function (data) {
        if (data.data.length == 0 || typeof data.data == "string") {
          alert("no data found");
        } else {
          let columnHeads = [];
          let columnDefs = [{
            "defaultContent": "-",
            "targets": "_all"
          }];

          $("table#" + activeTable + "Table").append("<thead><tr>");
          var objIndex = 0;
          for (var key in data.data[0]) {
            $("table#" + activeTable + "Table").append(
              '<th class="text-center">' + key + "</th>"
            );

            columnHeads.push({
              data: key
            });

            // data =
            //   '<a href="api/v1/' +
            //   Object.keys(row)[meta.col] +
            //   "/" +
            //   data +
            //   '" target="_blank">' +
            //   data +
            //   "</a>";

            if (
              Array.isArray(data.data[0][key]) &&
              data.data[0][key].length
            ) {
              columnDefs.push({
                targets: objIndex,
                "className": "text-center",
                "width": "4%",
                render: function (data, type, row, meta) {
                  let dataName = [];
                  let searchString = [];
                  if (type === "display") {
                    for (let i = 0; i < data.length; i++) {
                      let updateValue;
                      if (data[i].name) {
                        updateValue = `${data[i].name}`;
                        dataName.push(`<span class="content-id" title=${data[i]._id}>${data[i].name}</span>`);
                      } else if (data[i]._id) {
                        updateValue = data[i]._id;
                        dataName.push(data[i]._id);
                      } else {
                        updateValue = data[i]
                        dataName.push(data[i])
                      }

                      searchString.push(updateValue);
                    }
                  }

                  meta.settings.aoData[meta.row]._aFilterData ? meta.settings.aoData[meta.row]._aFilterData[meta.col] = searchString.toString() : "";

                  meta.settings.aoData[meta.row]._sFilterRow ? meta.settings.aoData[meta.row]._sFilterRow = meta.settings.aoData[meta.row]._aFilterData.toString() : meta.settings.aoData[meta.row]._sFilterRow

                  //console.log(meta.settings.aoData[meta.row])
                  return dataName;
                }
              });
            } else if (typeof data.data[0][key] === "object") {
              columnDefs.push({
                targets: objIndex,
                "className": "text-center",
                "width": "4%",
                render: function (data, type, row, meta) {
                  let dataName;
                  if (type === "display") {
                    if (data && data.name) {
                      dataName = `<span class="content-id" title=${data._id}>${data.name}</span>`;
                    } else if (data && data._id) {
                      dataName = data._id;
                    } else {
                      dataName = data;
                    }
                  }

                  meta.settings.aoData[meta.row]._aFilterData ? meta.settings.aoData[meta.row]._aFilterData[meta.col] = dataName : "";

                  meta.settings.aoData[meta.row]._sFilterRow ? meta.settings.aoData[meta.row]._sFilterRow = meta.settings.aoData[meta.row]._aFilterData.toString() : meta.settings.aoData[meta.row]._sFilterRow

                  return dataName;
                }
              });
            } else {
              columnDefs.push({
                targets: objIndex,
                "className": "text-center",
                "width": "4%"
              });
            }

            objIndex++;
          }

          $("table#" + activeTable + "Table").append(
            "</tr></thead><tbody></tbody>"
          );

          $("section#" + activeTable + "").css("display", "block");
          dt = $("#" + activeTable + "Table").dataTable({
            processing: true,
            data: data.data,
            columns: columnHeads,
            columnDefs: columnDefs,
            searchHighlight: true,
            "dom": '<lfi<t>p>',
            bDestroy: true
          });

          $("#" + activeTable + "Table").on('search.dt', function () {
            var value = $("#" + activeTable + "Table_filter input").val();
            $("#" + activeTable + "Table").removeHighlight();
            $("#" + activeTable + "Table").highlight(value);
          });
        }
      },
      error: function (jqxhr) {
        console.log(jqxhr.responseText);
        alert(jqxhr.responseText);
      }
    });
  });


  $('.submit-btn').click(function (e) {
    e.preventDefault();

    if (!$('#email').val()) {
      alert("Enter email id");
      return false;
    }

    if (!$('#pwd').val()) {
      alert("Enter password");
      return false;
    }

    let jsonData = {
      email: $('#email').val(),
      password: $('#pwd').val()
    };

    $.ajax({
      url: "http://localhost:3000/api/v1/user/authenticate/admin",
      type: "post",
      contentType: 'application/json',
      data: JSON.stringify(jsonData),
      success: function (data) {
        localStorage.setItem('data', JSON.stringify({
          "token": data.data.apiToken,
          "timestamp": new Date().getTime()
        }));

        window.location.href = '/dashboard';
      },
      error: function (jqxhr) {
        console.log(JSON.parse(jqxhr.responseText).error.message);
        alert(JSON.parse(jqxhr.responseText).error.message);
      }
    });

  });

  $('#logout').click(function (e) {
    localStorage.removeItem('data');
    window.location.href = '/';
  });

  $('#userTransaction').click(function (e) {
    window.open('/user-transaction', '_blank');
    return false;
  });

  $('#sanityTesting').click(function (e) {
    window.open('/sanity-testing', '_blank');
    return false;
  });

  $('#expertassign').click(function (e) {
    window.open('/expert-assign', '_blank');
    return false;
  });

});