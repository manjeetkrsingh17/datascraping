$(document).ready(function () {
    var token;
    if (window.location.pathname !== "/" && localStorage.getItem("data")) {
        token = localStorage.getItem("data").token;
        let expiry = localStorage.getItem("data").timestamp;

        if (new Date().getTime() - expiry >= 3600000) {
            localStorage.removeItem("data");
            window.location.href = "/";
        }
    } else if (window.location.pathname !== "/") {
        window.location.href = "/";
    }

    console.log(constants.SERVERAPI)

    loadChatRooms();

    function loadChatRooms() {
        $.ajax({
            url: constants.SERVERAPI + "/api/v4/expert/chat-room/list",
            headers: {
                'x-access-token': JSON.parse(localStorage.getItem('data')).token
            },
            success: function (data) {
                console.log(data)
                renderTable(data.data)
            },
            error: function (jqxhr) {
                console.log(jqxhr.responseText);
                alert(jqxhr.responseText);
            }
        });
    }



    $("#chatroom").delegate("span#ok_span", "click", function (e) {
        let chatRoomId = $(this).attr("chatRoomId");
        let expertElement = $(this).attr("expertId");
        let expertId = $('#' + expertElement).val()


        if (expertId == -1) {
            alert("Please select expert to assign");
            return false;
        } else {
            assignExpertToChatRoom(chatRoomId, expertId)
        }
    });

    function assignExpertToChatRoom(chatRoomId, expertId) {

        let jsonData = {
            expertId: expertId,
            chatRoomId: chatRoomId
        };
        $('#loader').show();

        $.ajax({
            url: constants.SERVERAPI + "/api/v4/expert/assign",
            headers: {
                'x-access-token': JSON.parse(localStorage.getItem('data')).token
            },
            type: "post",
            contentType: 'application/json',
            data: JSON.stringify(jsonData),
            success: function (data) {
                console.log(data)
                $('#loader').hide();
                alert("Assigned Successfully");
                loadChatRooms();
            },
            error: function (jqxhr) {
                console.log(jqxhr.responseText);
                alert(jqxhr.responseText);
            }
        });
    }

    function renderTable(data) {
        $('#chatroom tr').remove();

        if (data.chatRooms.length == 0) {
            $('#chatroom').append(`<li><a>No chat </a></li>`)
        }

        let expertDropdownItems = ""
        $.each(data.experts, function (index, obj) {
            expertDropdownItems = expertDropdownItems + `<option value='${obj._id}'>${obj.displayName}</option>`
        })


        $.each(data.chatRooms, function (index, obj) {

            let expertDropDown = `<div class="form-group" style="width:80%">
            <select class="form-control" id="selectbox_${obj._id}">
                <option value="-1">Select Expert:</option>
              ${expertDropdownItems}
            </select>
          </div><div><span id="ok_span" class="glyphicon glyphicon-ok" style="vertical-align:middle;margin-left:50%;cursor: pointer;" aria-hidden="true" chatRoomId='${obj._id}' expertId='selectbox_${obj._id}'></span></div>`;

            let msgs = "";
            if (obj.chatMessages && obj.chatMessages.length) {
                $.each(obj.chatMessages, function (index, msg) {
                    msgs = msgs + `<b>${index + 1}. </b> ${msg.body.message}`;

                    if (msg.body.response) {
                        msgs = msgs + `<span class="badge badge-success">${msg.body.response}</span>`;
                    }

                    msgs = msgs + `<br/>`
                })
            }


            $('#chatroom').append(`<tr>
            <th scope="row">${index+1}</th>
            <td>${obj.userProfile.firstName}</td>
            <td>${obj.userProfile.email}</td>
            <td>${obj.userProfile.mobileNumber}</td>
            <td>${obj.chatArea && obj.chatArea.name ?obj.chatArea.name:""}</td>
            <td>${msgs}</td>
            <td>${moment(obj.createdAt).fromNow()}</td>
            <td style="display:flex;">${expertDropDown}</td>
        </tr>`)
        });
    }

})